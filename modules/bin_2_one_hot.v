// converts binary encoding to one hot encoding

module bin_2_one_hot(
	input      [2:0]    bin_i,
	input            enable_i,
	output reg [4:0]    hot_o
);

	// combinational logic
	always @(*) begin
		if (enable_i) begin
			case(bin_i)
				3'd0    : hot_o = 5'b00001;
				3'd1    : hot_o = 5'b00010;
				3'd2    : hot_o = 5'b00100;
				3'd3    : hot_o = 5'b01000;
				3'd4    : hot_o = 5'b10000;
				// for debugging
				default : hot_o = 5'b11111;
			endcase
		end else begin
			hot_o = 5'b00000;
		end
	end
endmodule
