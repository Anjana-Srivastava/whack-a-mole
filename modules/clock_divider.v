module clock_divider(
	input      clock_i,
	output reg clock_o);
	
	reg [27:0] counter;
	reg [27:0] counter_int;
	
	reg clock_o_int;
	
	parameter DIVISOR = 28'hC350;
	
	// sequential logic
	always @(posedge clock_i) begin
		counter <= counter_int;
		clock_o <= clock_o_int;
	end
	
	// combinational logic
	always @(*) begin
		clock_o_int = (counter < DIVISOR/2) ? 1 : 0;
		counter_int = (counter >= (DIVISOR - 1)) ? 0 : counter + 1;
	end
endmodule