`include "count_incrementer.v"

module count_incrementer_top(
	input           clock_i,
	input [4:0]     moles_i,
	input [4:0]   buttons_i,
	output      increment_o
);
	wire clock_toggle;
	wire button_toggle;
	reg  hit;

	count_incrementer inc (
		    .clock_i (     clock_i ),
		      .hit_i (       hit   ),
		.increment_o ( increment_o )
	);

	always @(*) 
		      hit   = |( moles_i & buttons_i );

endmodule
