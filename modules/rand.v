// implementation of 3 bit random number generator spanning from
// 0 to 4 using 3 10 bit linear feadback shift registers

`include "rand_lfsr.v"

module rand(
	input             init_i,
	output reg [2:0]   num_o
);
	// needed to capture unbounded random number
	wire [2:0]  num_int;

	// LFSR intantiation
	rand_lfsr gen (
		.increment_i( init_i   ),
		      .bit_o(  num_int )
	);

	// combinational logic
	always @(posedge init_i)
		num_o = { (num_int[2] & !(num_int[1] | num_int[0])), num_int[1:0] };
endmodule
