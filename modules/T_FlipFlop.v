`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/02/2021 01:09:34 PM
// Design Name: 
// Module Name: T_FlipFlop
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//T Flip-Flop
module T_FlipFlop(
    input clock, reset, T,
    output reg Q
    );
    
    always @ (posedge clock, negedge reset) begin
        if (!reset) Q <= 1'b0;
        else begin
            case (T)
                1'b0    : Q <= Q;       //no change
                1'b1    : Q <= (~Q);    //complement
            endcase
        end
    end
endmodule
