`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/02/2021 12:59:27 PM
// Design Name: 
// Module Name: BCD_Counter_4bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//4-bit Binary Coded Decimal (BCD) Counter
module BCD_Counter_4bit(
    input clock,            //increments the counter on the positive edge
    input reset,            //resets the count to 0000
    output [3:0] bcd_count, //the BCD count
    output next_count       //clock input for subsequent cascaded BCD_Counter_4bit
    );
    
    wire t1, t2, t4, t8;
    
    assign t1 = 1'b1;
    assign t2 = (~bcd_count[3]) && bcd_count[0];
    assign t4 = bcd_count[1] && bcd_count[0];
    assign t8 = (bcd_count[3] && bcd_count[0]) || (bcd_count[2] && bcd_count[1] && bcd_count[0]);
    
    T_FlipFlop tff1 (clock, reset, t1, bcd_count[0]);    //Q1
    T_FlipFlop tff2 (clock, reset, t2, bcd_count[1]);    //Q2
    T_FlipFlop tff4 (clock, reset, t4, bcd_count[2]);    //Q4
    T_FlipFlop tff8 (clock, reset, t8, bcd_count[3]);    //Q8
    
    assign next_count = ~(bcd_count[3] && bcd_count[0]);
    
endmodule
