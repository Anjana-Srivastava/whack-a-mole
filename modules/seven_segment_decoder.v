module seven_segment_decoder(
	input      [3:0] hex_num_i,
	output reg [6:0]  decode_o);
	
	parameter num_0 = ~(7'b1111110);
	parameter num_1 = ~(7'b0110000);
	parameter num_2 = ~(7'b1101101);
	parameter num_3 = ~(7'b1111001);
	parameter num_4 = ~(7'b0110011);
	parameter num_5 = ~(7'b1011011);
	parameter num_6 = ~(7'b1011111);
	parameter num_7 = ~(7'b1110000);
	parameter num_8 = ~(7'b1111111);
	parameter num_9 = ~(7'b1111011);
	parameter num_E = ~(7'b1001111);

	always @(*) begin
	
		case (hex_num_i)
			4'd0    : decode_o = num_0;
			4'd1    : decode_o = num_1;
			4'd2    : decode_o = num_2;
			4'd3    : decode_o = num_3;
			4'd4    : decode_o = num_4;
			4'd5    : decode_o = num_5;
			4'd6    : decode_o = num_6;
			4'd7    : decode_o = num_7;
			4'd8    : decode_o = num_8;
			4'd9    : decode_o = num_9;
			default : decode_o = num_E;
		endcase
	end

endmodule
