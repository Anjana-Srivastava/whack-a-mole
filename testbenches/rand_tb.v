`include "rand.v"

module rand_tb;

	reg        init_i;
	reg  [6:0]  count;
	wire [2:0]  num_o;

	parameter MAX = 100;

	rand tb (
		.init_i ( init_i ),
		 .num_o (  num_o )
	);

	initial begin
		init_i = 0;
		count  = 0;
	end

	always begin
		#1
		init_i = !init_i;
		#1
		init_i = !init_i;
		#1
		$display("%d", num_o);
		if (count != MAX)
			count = count + 1;
		else
			$finish;
	end
endmodule
