`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/02/2021 03:34:53 PM
// Design Name: 
// Module Name: T_FlipFlop_TEST
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module T_FlipFlop_TEST(
    );
    
    reg clock, reset, T;
    wire Q;
    
    T_FlipFlop tff (clock, reset, T, Q);
    
    initial
    begin
        clock = 1'b0;
        reset = 1'b1;   //active low reset
    end
    
    initial begin
        #20 T = 1'b1;
        #80 reset = 1'b0;
        #30 reset = 1'b1;
        #100 T = 1'b0;
        #100 T = 1'b1;
        #100 $finish;
    end
    
    always #10 clock = ~clock;
    
endmodule
