`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/02/2021 03:08:12 PM
// Design Name: 
// Module Name: BCD_Counter_4bit_TEST
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//Test for 4-bit Binary Coded Decimal (BCD) Counter
module BCD_Counter_4bit_TEST(
    );
    
    reg clock, reset;
    wire [7:0] bcd_count;           //8-bit bcd_count because two counters are cascaded
    wire next_count1, next_count2;  //the next_count for each cascaded 4-bit bcd counter
    
    BCD_Counter_4bit bcd4b1 (clock, reset, bcd_count[3:0], next_count1);
    BCD_Counter_4bit bcd4b2 (next_count1, reset, bcd_count[7:4], next_count2); 
    
    initial
    begin
        clock = 1'b0;
        reset = 1'b1;   //active low reset
    end
    
    initial begin
        #30 reset = 1'b0;
        #30 reset = 1'b1;
        #650 reset = 1'b0;
        #50 reset = 1'b1;
        #100$finish;
    end
    
    always #10 clock = ~clock;
endmodule
