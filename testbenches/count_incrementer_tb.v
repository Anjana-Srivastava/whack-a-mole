`include "count_incrementer.v"

module count_incrementer_tb;

	reg clock_i;
	reg   hit_i;

	wire increment_o;

	count_incrementer tb (
		    .clock_i (     clock_i ),
		      .hit_i (       hit_i ),
		.increment_o ( increment_o )
	);

	initial begin
		$dumpfile("count_incrementer_tb.vcd");
		$dumpvars(0,count_incrementer_tb);
		clock_i = 0;
		  hit_i = 0;
		#10
		clock_i = ~clock_i;
		#10
		clock_i = ~clock_i;
		#10
		clock_i = ~clock_i;
		#10
		clock_i = ~clock_i;
		#2
		hit_i   = 1;
		#2
		hit_i   = 0;
		#2
		hit_i   = 1;
		#2
		hit_i   = 0;
		#2
		clock_i = ~clock_i;
		#10
		clock_i = ~clock_i;
		#10
		clock_i = ~clock_i;
		#10
		clock_i = ~clock_i;
		#5
		$finish;
	end

endmodule
